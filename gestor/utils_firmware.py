# -*- coding: utf-8 -*-
'''
Gestor de nodos para LaOtraRed La Paz - El Alto

 Copyright (C) 2017  Rodrigo Garcia

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
import subprocess
import os
import re

from gestor.configs import RUTA_SCRIPT_GENERADOR_FIRMWARES
from gestor.configs import RUTA_IMAGENES_FIRMWARE_GENERADAS

# se utiliza el script generador de firmware:
# https://git.laotrared.net/LaOtraRed-dev/generador-firmwares-script

def crear_imagenFirmware(modelo_router, bloque_ipv4, cidr):
    ''' Genera la imagen de firmware ejecutando el script
    https://git.laotrared.net/LaOtraRed-dev/generador-firmwares-script/chef.sh
    
    Retorna el nombre resultante de la imagen generada
    Si ocurre un error retorna la cadena "Error"
    '''
    print ("---- Generando imagen de firmware -----")
    print ("comando: "+str([RUTA_SCRIPT_GENERADOR_FIRMWARES+\
                               "llamar_chef.sh",\
                               RUTA_SCRIPT_GENERADOR_FIRMWARES,\
                               modelo_router,
                               bloque_ipv4+"/"+str(cidr)]))

    res = subprocess.call([RUTA_SCRIPT_GENERADOR_FIRMWARES+\
                           "llamar_chef.sh",\
                           RUTA_SCRIPT_GENERADOR_FIRMWARES,\
                           modelo_router,
                           bloque_ipv4+"/"+str(cidr)])

    print ("resultado:"+str(res))
    
    if res != 0:
        return "Error"
    
    return RUTA_IMAGENES_FIRMWARE_GENERADAS+modelo_router+"_"+bloque_ipv4


def carpetas_descargaFirmware(bloque_ipv4=None, id_nodo=-1):
    ''' Devuelve una lista con todas las carpeta(s) donde se encuentran 
    las imagenes de firmware generadas para el nodo con el bloque ipv4
    publico dado.

    Se devuleve una lista vacia en caso de no encontrar la carpeta
    '''
    ipv4 = bloque_ipv4.split("/")[0]
    try:
        ow = os.walk(RUTA_IMAGENES_FIRMWARE_GENERADAS)
        p , dirs , archs = ow.next()
        carpetas = []
        # filtrar solo carpetas pertenecientes al bloque_ipv4
        # notar que los nombres de carpetas generadas terminan con la ipv4
        pattern = re.compile(ipv4+"$")
        for d in dirs:
            if re.search(pattern,d) is not None:
                carpetas.append(d)

        return carpetas
    except OSError:
        print ("Error al ejecutar os.walk("+RUTA_IMAGENES_FIRMWARE_GENERADAS+")")
        return []

def listar_carpetaFirmware(carpeta=None):
    ''' Devuelve dos listas con archivos y directorios dentro la carpeta
    con el nombre dado, solo busca dentro el directorio:
    RUTA_IMAGENES_FIRMWARE_GENERADAS
    Retorna una lista vacia si no encuentra
    '''
    try:
        ow = os.walk(RUTA_IMAGENES_FIRMWARE_GENERADAS + carpeta)
        p, dirs, archs = ow.next()
        return (dirs, archs)
        
    except:
        print (("Error al ejecutar os.walk("+ \
                RUTA_IMAGENES_FIRMWARE_GENERADAS+"/"+carpeta + ")"))
        return []

# -*- coding: utf-8 -*-
'''
Gestor de nodos para LaOtraRed La Paz - El Alto

 Copyright (C) 2017  Rodrigo Garcia

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
from sqlalchemy import Column, Integer, String, Float, Boolean
from sqlalchemy import ForeignKey
from database.database import Base

class Ubicacion(Base):
    __tablename__ = 'ubicacion'
    sqlite_autoincrement=True
    id = Column(Integer, primary_key=True)
    localidad = Column(String(200))
    zona = Column(String(250))
    direccion = Column(String(200))
    url_mapa = Column(String(1000))
    iframe_mapa = Column(String(2000))
    elevacion_aproximada = Column(Integer)
    latitud = Column(Float())
    longitud = Column(Float())
    is_public = Column(Boolean(create_constraint=True))    

    def __init__(self, localidad='', zona='',
                 direccion='', url_mapa='',
                 iframe_mapa='', elevacion_aproximada='',
                 latitud=16.0, longitud=16.0,
                 is_public=True):
        self.localidad= localidad
        self.zona = zona
        self.direccion = direccion
        self.url_mapa = url_mapa
        self.iframe_mapa = iframe_mapa
        self.elevacion_aproximada = elevacion_aproximada
        self.latitud = latitud
        self.longitud = longitud
        self.is_public = is_public
    
    def __repr__(self):
        return '<Ubicacion(%s) %s, %s, %s publica:%s> '\
            %(str(self.id), self.localidad, self.zona, self.direccion,\
              str(self.is_public))
    
class Nodo(Base):
    __tablename__ = 'nodo'
    sqlite_autoincrement=True
    id = Column(Integer, primary_key=True)
    ubicacion_id = Column(Integer, ForeignKey('ubicacion.id'))

    nombre = Column(String(200), unique=True)
    bloque_ipv4 = Column(String(19), unique=True)
    bloque_ipv4_privado = Column(String(19), unique=True)
    ipv4_network_addr = Column(String(16), unique=True)
    ipv4_cidr = Column(Integer)
    ipv4_cidr_privado = Column(Integer)
    bloque_ipv6 = Column(String(164), unique=True)
    ipv6_network_addr = Column(String(164), unique=True)
    ipv6_cidr = Column(Integer)
    is_active = Column(Boolean(create_constraint=False))
    is_confirmed = Column(Boolean(create_constraint=False))

    url_descarga = Column(String(500), unique=True)
    fecha_creacion = Column(String(20)) # se guardan en formato YYYY-MM-DD hh:mm:ss
    
    descripcion = Column(String(2599))
    clave_edicion = Column(String(500))
    
    nombres_responsable = Column(String(200))
    apellidos_responsable = Column(String(200))
    email = Column(String(254))
    telf = Column(String(30))

    def __init__(self, responsable_id=1, ubicacion_id=1,\
                 nombre = "-",
                 bloque_ipv4="-", ipv4_network_addr="-",\
                 bloque_ipv4_privado = "-", ipv4_cidr_privado=27,
                 ipv4_cidr=29, bloque_ipv6="-", \
                 ipv6_network_addr="-",\
                 ipv6_cidr=128,\
                 is_active=False, is_confirmed=False,\
                 url_descarga="sin ulr",\
                 fecha_creacion = "no valida",\
                 descripcion="-", clave_edicion="Sin Clave",\
                 nombres_responsable="-",\
                 apellidos_responsable="-",\
                 email="-",\
                 telf="-",\
    ):
        self.responsable_id = responsable_id
        self.ubicacion_id = ubicacion_id
        self.nombre = nombre
        self.bloque_ipv4 = bloque_ipv4
        self.bloque_ipv4_privado = bloque_ipv4_privado
        self.ipv4_network_addr = ipv4_network_addr
        self.ipv4_cidr = ipv4_cidr
        self.ipv4_cidr_privado = ipv4_cidr_privado
        self.bloque_ipv6 = bloque_ipv6
        self.ipv6_network_addr = ipv6_network_addr
        self.ipv6_cidr = ipv6_cidr
        self.is_active = is_active
        self.is_confirmed = is_confirmed
        self.descripcion = descripcion
        self.clave_edicion = clave_edicion
        self.url_descarga = url_descarga
        self.fecha_creacion = fecha_creacion
        self.nombres_responsable = nombres_responsable.lower()
        self.apellidos_responsable = apellidos_responsable.lower()
        self.email = email.lower()
        self.telf = telf
    
    def __repr__(self):
        return '<Nodo(%s) %s, %s, activ: %s, confir: %s <%s>'\
            % (str(self.id), self.nombre, self.bloque_ipv4, str(self.is_active),\
               str(self.is_confirmed), self.email)

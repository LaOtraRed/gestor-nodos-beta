#!/usr/bin/env python
# -*- coding: utf-8 -*-

# aqui se cargan las configs y variables globales dentro
# el contexto de la app en src/
from flask import Flask
import sys

app = Flask(__name__, static_url_path='/static')

import sqlite3

from flask import Flask, request, session, g, redirect, url_for, \
    abort, render_template, flash

import views
import configs

from database.database import db_session, db_session
from database.database import engine

@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()




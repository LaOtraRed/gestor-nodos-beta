# -*- coding: utf-8 -*-
'''
Gestor de nodos para LaOtraRed La Paz - El Alto

 Copyright (C) 2017  Rodrigo Garcia

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
# configuraciones

# Poner esto en False cuando se esta en produccion
#DEBUG=False
DEBUG=True

DEPLOY=not DEBUG

SECRET_KEY="llave secreta de desarrollo"

USUARIO_ADMINISTRADOR="nombre usuario administrador"
CLAVE_ADMINISTRADOR="clave de usuario administrador"

# base de datos (sqlite3)
#sqlite://<nohostname>/<path>
DATABASE_NAME="sqlite:///database/gestor_nodos_lor.db"
DATABASE_USERNAME="usuario"
DATABASE_PASSWORD="password de usuario de base de datos"

# email desde el que se envian notificaciones
EMAIL_HOST = "correo.laotrared.net"
EMAIL_PORT = "587"
EMAIL_HOST_USER = "nodos"
EMAIL_HOST_PASSWORD = "password del email del gestor de nodos"
EMAIL_USE_TLS = True
EMAIL_USE_SSL = False
EMAIL_SSL_CERTFILE = "/ruta/hacia/archivo/de/certificado/ssl"
EMAIL_SSL_KEYFILE = "/ruta/hacia/clave/ssl"
EMAIL_TIMEOUT = 60

# script generador de firmwares
RUTA_SCRIPT_GENERADOR_FIRMWARES="/ruta/hacia/el/script"
RUTA_IMAGENES_FIRMWARE_GENERADAS="/ruta/hacia/las/imagenes/de/firmware/generadas"

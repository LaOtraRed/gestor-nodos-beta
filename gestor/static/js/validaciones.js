function validarAgregarNodo()
{
    var s = document.getElementById("id_modelos");
    var modelo = s.options[s.selectedIndex].value;
    s = document.getElementById("id_ips_publicas");
    var ips_publicas = s.options[s.selectedIndex].value;
    s = document.getElementById("id_ips_privadas");
    var ips_privadas = s.options[s.selectedIndex].value;

    var nombres = document.getElementById("id_nombres").value;
    var apellidos = document.getElementById("id_apellidos").value;
    var nombre_nodo = document.getElementById("id_nombre_nodo").value;
    var clave = document.getElementById("id_clave").value;

    if (modelo == "" || modelo.length < 5 || modelo == "----------2.4 Ghz--------" || modelo == "----------DUAL BAND (2.4 y 5 Ghz)--------" || modelo == "----------5 Ghz----------") {
	document.getElementById("res_modelos").innerHTML = 
	'<img src="/static/imgs/incorrecto.png" width="18" heigth="18">';
	document.getElementById("id_modelos").focus();
	return false;
    }
    else {
	document.getElementById("res_modelos").innerHTML = 
	'<img src="/static/imgs/correcto.png" width="18" heigth="18">';
    }

    if (ips_publicas == "14" || ips_publicas == "30" || ips_publicas == "62" || 
       ips_publicas == "126" || ips_publicas == "254") {
	document.getElementById("res_ips_publicas").innerHTML =
	'<img src="/static/imgs/correcto.png" width="18" heigth="18">';
    }
    else {
	document.getElementById("res_ips_publicas").innerHTML =
	'<img src="/static/imgs/incorrecto.png" width="18" heigth="18">';
	document.getElementById("id_ips_publicas").focus();
	return false;
    }

    if (ips_privadas == "30" || ips_privadas == "62" || ips_privadas == "126") {
	document.getElementById("res_ips_privadas").innerHTML =
	'<img src="/static/imgs/correcto.png" width="18" heigth="18">';
    }
    else {
	document.getElementById("res_ips_privadas").innerHTML =
	'<img src="/static/imgs/incorrecto.png" width="18" heigth="18">';
	document.getElementById("ips_privadas").focus();
	return false;
    }
    
    if (nombres.length < 3){
	document.getElementById("res_nombres").innerHTML =
	'<img src="/static/imgs/incorrecto.png" width="18" heigth="18">';
	document.getElementById("id_nombres").focus();
	return false;
    }
    else {
	document.getElementById("res_nombres").innerHTML =
	'<img src="/static/imgs/correcto.png" width="18" heigth="18">';
    }

    if(apellidos.length < 3){
	document.getElementById("res_apellidos").innerHTML =
	'<img src="/static/imgs/incorrecto.png" width="18" heigth="18">';
	document.getElementById("id_apellidos").focus();
	return false;
    }
    else {
	document.getElementById("res_apellidos").innerHTML =
	'<img src="/static/imgs/correcto.png" width="18" heigth="18">';
    }

    // agregar funcion para comprobar email
    // ..

    if(nombre_nodo.length < 3) {
	document.getElementById("res_nombre_nodo").innerHTML =
	'<img src="/static/imgs/incorrecto.png" width="18" heigth="18">';
	document.getElementById("id_nombre_nodo").focus();
	return false;
    }
    else {
	document.getElementById("res_nombre_nodo").innerHTML =
	'<img src="/static/imgs/correcto.png" width="18" heigth="18">';
    }

    if(clave.length < 4){
	document.getElementById("res_clave").innerHTML =
	'<img src="/static/imgs/incorrecto.png" width="18" heigth="18">';
	document.getElementById("id_clave").focus();
	return false
    }
    else {
	document.getElementById("res_clave").innerHTML =
	'<img src="/static/imgs/correcto.png" width="18" heigth="18">';
    }
    
    return true;
    // Agregar envio de formulario
    //document.getElementById("myForm").submit();

    document.getElementById("agregar_nodo_form").submit();
};


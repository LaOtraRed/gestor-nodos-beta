from setuptools import setup

setup(
    name= 'gestor-nodos-lor' ,
    packages=[ 'gestor' ],
    include_package_data=True,
    install_requires=[
        'gestor' ,
    ],
)

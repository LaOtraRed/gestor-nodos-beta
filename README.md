Herramienta de gestión de nodos para el proyecto LaOtraRed La Paz - El Alto.

Esta plataforma web tiene las siguientes funciones:

* Mantener un informe en tiempo real de los nodos registrados con sus detalles tales como:
   * IPs asignadas.
   * Ubicación del nodo (según se proporcione).
   * Datos del dueño de cada nodo (según se proporcione)
   * Descripción de cada nodo.
* Herramienta de creación de firmwares para unirse a LaOtraRed.
   * Asignación de direcciones IP siguiendo la [política de asignación de IPs](https://wiki.lapaz.laotrared.net/redlibre/politica_de_asignacion_de_ips).
   * Generación imagen de firmware con las configuraciones necesarias para conectarse a LaOtraRed con sólo encender el enrutador, siguiendo [esto](https://wiki.lapaz.laotrared.net/guias/configuracion_de_enrutadores_y_dispositivos_de_red).
   * Interfaz cómoda de creación de imágenes de firmware para ciertos modelos de routers, las configuraciones para la generación de firmwares para cada modelo las extrae de https://git.laotrared.net/LaOtraRed-dev/generador-firmwares-script/src/master/configuraciones_chef.
   * Asignación única de bloque de IPs por nodo.
* Mapa de nodos
   * Mapa de la ubicación de los nodos en la red.

## Organización del código ##

   * `/Docu`: Documentación, diagramas y especificaciones.
   * `/database`: Módulos de la base de datos.
   * `/gestor`: Aplicación web de gestión de nodos y utilitarios para creación de firmware.
   * `/logs`: Módulo de registros
   * `/mapas`: Módulo de carga de mapas (no implementado)
   * `/dominios`: Módulo de registro de dominios (no implementado)

**LICENCIA**: AGPL

Desarrollado con python 2.7, Flask (0.12) y bootstrap 3.3.7.

 -- Grupo de estudio de redes libres (2017) --

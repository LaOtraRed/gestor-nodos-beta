'''
Gestor de nodos para LaOtraRed La Paz - El Alto

 Copyright (C) 2017  Rodrigo Garcia

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
from flask import Flask

from gestor.main import app as application

from database.database import init_db, db_session

import gestor.configs as configs

# registrando blueprints
from gestor.views import gestor as gestor_module
application.register_blueprint(gestor_module)


init_db()
application.debug = configs.DEBUG
application.secret_key = configs.SECRET_KEY

if __name__ == '__main__':
    application.run(host='0.0.0.0')

